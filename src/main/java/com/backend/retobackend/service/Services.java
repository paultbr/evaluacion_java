package com.backend.retobackend.service;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

public class Services {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(Services.class);
	public final String url = "https://dev.interseguro.pe/payment-api/api/v2/session";
	
	public static String postRequest(String uri, String data) {
		HttpURLConnection urlConnection;
		String result = null;
		try {
			urlConnection = (HttpURLConnection) ((new URL(uri).openConnection()));
			urlConnection.setDoOutput(true);
			urlConnection.setRequestProperty("Content-Type", "application/json");
			urlConnection.setRequestProperty("Content-Type", "application/json");
			urlConnection.setRequestProperty("X-Apikey", "4e657008-bb87-4e89-81bc-0fb62879b497");
			urlConnection.setRequestProperty("X-Provider", "NIUBIZ");
			urlConnection.setRequestMethod("POST");
			urlConnection.connect();

			OutputStream outputStream = urlConnection.getOutputStream();
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
			writer.write(data);
			writer.close();
			outputStream.close();

			BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));
			String line = null;
			StringBuilder sb = new StringBuilder();
			while ((line = bufferedReader.readLine()) != null) {
				sb.append(line);
			}
			bufferedReader.close();
			result = sb.toString();
		} catch (UnsupportedEncodingException ex) {
			registrarError(ex);
		} catch (IOException ex) {
			registrarError(ex);
		}
		return result;
	}

	private static void registrarError(Exception ex) {
		ex.printStackTrace();
		LOGGER.error(ex.getMessage());
	}

}
