package com.backend.retobackend.service;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.backend.retobackend.service.Services;
import com.backend.retobackend.service.interfaces.*;

import com.backend.retobackend.model.Result;
import com.backend.retobackend.model.sessionModel;

@Service
public class evaluacionService extends Services implements IevaluacionService{
	

	public Result examen() {
		try {
		    String url = this.url;
		    String result=Services.postRequest(url, "");//recuperamos la sessionKey de nuestro procedimiento postRequest
			return new Result(1, "Message", "SUCCES",result);
		}
		catch (Exception e) {
			return new Result(0, "Message", e.getMessage().toString());
		}
		
	}

}
