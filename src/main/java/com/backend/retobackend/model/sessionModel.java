package com.backend.retobackend.model;

public class sessionModel {
	
	private double amount;
	private String currency;
	private String channel;
	private String recurrentMaxAmount;
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getRecurrentMaxAmount() {
		return recurrentMaxAmount;
	}
	public void setRecurrentMaxAmount(String recurrentMaxAmount) {
		this.recurrentMaxAmount = recurrentMaxAmount;
	}
	
	
}
