package com.backend.retobackend.model;

public class Result {
	
	private int code;
	private String title;
	private String message;
	private Object data;
	
	
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public Result(int codeParameter, String titleParameter, String messageParameter) {
		this.setCode(codeParameter);
		this.setTitle(titleParameter);
		this.setMessage(messageParameter);
	}
	public Result(int codeParameter,String titleParameter, String messageParameter,Object resultParameter) {
		this.setCode(codeParameter);
		this.setTitle(titleParameter);
		this.setMessage(messageParameter);
		this.setData(resultParameter);
	}

}
